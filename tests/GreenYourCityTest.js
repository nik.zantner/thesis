const helicopterInitialization = async function (t) {
    let helicopter = t.getSprites(sprite => sprite.name.includes('Helicopter') || sprite.name.includes('helicopter') || (sprite.x < 0 && sprite.y > 100))[0];
    t.assert.ok(helicopter.visible, 'helicopter must be visible');
    t.end();
};

const dropsInitialization = async function (t) {
    let drop = t.getSprites(sprite => sprite.name.includes('water') || sprite.name.includes('drop') || sprite.name.includes('Water') || sprite.name.includes('Drop'))[0];
    t.assert.ok(!drop.visible, 'water drops must be invisible');
    t.end();
};

const flowerInitialization = async function (t) {
    let flowers = t.getSprites(sprite => sprite.name.includes('flower') || sprite.name.includes('Flower'));
    t.assert.ok(flowers.length === 4, 'there are not exactly 4 flowers' + flowers.length);
    let flower = flowers[0];
    t.assert.ok(flower.visible, 'flower 1 must be visible');
    flower = flowers[1];
    t.assert.ok(flower.visible, 'flower 2 must be visible');
    flower = flowers[2];
    t.assert.ok(flower.visible, 'flower 3 must be visible');
    flower = flowers[3];
    t.assert.ok(flower.visible, 'flower 4 must be visible');
    t.end();
};

const scoreInitialization = async function (t) {
    const stage = t.getStage();
    let score = stage.getVariables(variable => variable.name.includes('score') || variable.name.includes('Score'))[0];
    t.assert.ok(score.value === 0, 'Score must be 0');
    t.end();
};

const helicopterMove = async function (t) {
    let helicopter = t.getSprites(sprite => sprite.name.includes('Helicopter') || sprite.name.includes('helicopter') || (sprite.x < 0 && sprite.y > 100))[0];
    await t.runForSteps(5);
    let helicopterX = helicopter.x;
    let moved = false;
    t.onSpriteMoved(() => {
        if (!moved && helicopter.x > helicopterX) {
            moved = true;
        }
    });
    await t.runUntil(() => moved === true, 5000);
    t.assert.ok(moved === true, 'Boat did not move right');
    t.end();
};

const helicopterMoveNextRow = async function (t) {
    let helicopter = t.getSprites(sprite => sprite.name.includes('Helicopter') || sprite.name.includes('helicopter') || (sprite.x < 0 && sprite.y > 100))[0];
    await t.runForSteps(5);
    let helicopterY = helicopter.y;
    let touchedEdge = false;
    t.onSpriteMoved(() => {
        if (!touchedEdge && helicopter.isTouchingEdge()) {
            touchedEdge = true;
        }
    });
    await t.runUntil(() => touchedEdge === true, 10000);
    t.assert.ok(touchedEdge === true, 'Helicopter did not touch edge');
    await t.runForSteps(40);
    t.assert.ok(helicopter.y < helicopterY, 'Helicopter did not move down');
    t.end();
};

const flowerOneGrowing = async function (t) {
    await flowerGrowing(t, 1);
};

const flowerTwoGrowing = async function (t) {
    await flowerGrowing(t, 2);
};

const flowerThreeGrowing = async function (t) {
    await flowerGrowing(t, 3);
};

const flowerFourGrowing = async function (t) {
    await flowerGrowing(t, 4);
};

const flowerGrowing = async function (t, numberOfFlower) {
    let helicopter = t.getSprites(sprite => sprite.name.includes('Helicopter') || sprite.name.includes('helicopter') || (sprite.x < 0 && sprite.y > 100))[0];
    await t.runForSteps(2);
    let flowers = t.getSprites(sprite => sprite.name.includes('flower') || sprite.name.includes('Flower'));
    let flower = flowers[numberOfFlower - 1];
    let flowerX = flower.x;
    let flowerY = flower.y;
    let rightPosition = false;
    t.onSpriteMoved(() => {
        if (helicopter.x <= flowerX + 1 && helicopter.x >= flowerX - 1) {
            rightPosition = true;
        }
    });
    await t.runUntil(() => rightPosition === true, 10000);
    t.assert.ok(rightPosition === true, 'right position was not found');
    t.addInput(0, {device: 'keyboard', key: 'space', isDown: true, duration: 50});
    await t.runForSteps(10);
    await t.runUntil(() => flower.isTouchingSprite(t.getSprites(sprite => sprite.name.includes('water') || sprite.name.includes('drop') || sprite.name.includes('Water') || sprite.name.includes('Drop'))[0].name) === true, 10000);
    await t.runForSteps(40);
    t.assert.ok(flower.y > flowerY, 'flower ' + numberOfFlower + ' did not grow' + flower.name + flower.x);
    t.end();
};

module.exports = [
    {
        test: helicopterInitialization,
        name: 'initialize helicopter',
        description: '',
        categories: []
    },
    {
        test: dropsInitialization,
        name: 'initialize water drops',
        description: '',
        categories: []
    },
    {
        test: flowerInitialization,
        name: 'initialize flowers',
        description: '',
        categories: []
    },
    {
        test: scoreInitialization,
        name: 'initialize score value',
        description: '',
        categories: []
    },
    {
        test: helicopterMove,
        name: 'helicopter move',
        description: '',
        categories: []
    },
    {
        test: helicopterMoveNextRow,
        name: 'helicopter moves next row',
        description: '',
        categories: []
    },
    {
        test: flowerOneGrowing,
        name: 'flower 1 growing',
        description: '',
        categories: []
    },
    {
        test: flowerTwoGrowing,
        name: 'flower 2 growing',
        description: '',
        categories: []
    },
    {
        test: flowerThreeGrowing,
        name: 'flower 3 growing',
        description: '',
        categories: []
    },
    {
        test: flowerFourGrowing,
        name: 'flower 4 growing',
        description: '',
        categories: []
    }


];
