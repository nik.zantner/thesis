const devinInitialization = async function (t) {
    let devin = t.getSprites(sprite => sprite.name.includes('Devin') || sprite.name.includes('devin') || (sprite.x < -85 && sprite.y < -40 && sprite.x > -95 && sprite.y > -50))[0];
    t.assert.ok(devin.visible, 'Devin must be visible');
    t.end();
};

const alexInitialization = async function (t) {
    let alex = t.getSprites(sprite => sprite.name.includes('Alex') || sprite.name.includes('alex') || (sprite.x < -170 && sprite.y < -40 && sprite.x > -180 && sprite.y > -50))[0];
    t.assert.ok(alex.visible, 'Alex must be visible');
    t.end();
};

const rocksInitialization = async function (t) {
    let rocks = t.getSprites(sprite => sprite.name.includes('Rocks') || sprite.name.includes('rocks') || (sprite.x < 15 && sprite.y < -105 && sprite.x > 0 && sprite.y > -120))[0];
    t.assert.ok(rocks.visible, 'Rocks must be visible');
    t.end();
};

const laptopInitialization = async function (t) {
    let laptop = t.getSprites(sprite => sprite.name.includes('Laptop') || sprite.name.includes('laptop') || (sprite.x < 15 && sprite.y < -80 && sprite.x > 0 && sprite.y > -95))[0];
    t.assert.ok(laptop.visible, 'Laptop must be visible');
    t.end();
};

const antennaeInitialization = async function (t) {
    let antennae = t.getSprites(sprite => sprite.name.includes('Antennae') || sprite.name.includes('antennae') || (sprite.x < -85 && sprite.y < 85 && sprite.x > -97 && sprite.y > 65))[0];
    t.assert.ok(antennae.visible, 'Antennae must be visible');
    t.end();
};

const sunglassesInitialization = async function (t) {
    let sunglasses = t.getSprites(sprite => sprite.name.includes('Sunglasses') || sprite.name.includes('sunglasses') || (sprite.x < -170 && sprite.y < 55 && sprite.x > -185 && sprite.y > 40))[0];
    t.assert.ok(sunglasses.visible, 'Sunglasses must be visible');
    t.end();
};

const bowtieInitialization = async function (t) {
    let bowtie = t.getSprites(sprite => sprite.name.includes('Bowtie') || sprite.name.includes('bowtie') || (sprite.x < -80 && sprite.y < 30 && sprite.x > -91 && sprite.y > 19))[0];
    t.assert.ok(bowtie.visible, 'Bowtie must be visible');
    t.end();
};

const helicopterInitialization = async function (t) {
    let helicopter = t.getSprites(sprite => sprite.name.includes('Helicopter') || sprite.name.includes('helicopter') || (sprite.x < 174 && sprite.y < -120 && sprite.x > 154 && sprite.y > -140))[0];
    t.assert.ok(helicopter.visible, 'helicopter must be visible');
    t.end();
};

const helicopterLooping = async function (t) {
    await t.runForSteps(50);
    let helicopter = t.getSprites(sprite => sprite.name.includes('Helicopter') || sprite.name.includes('helicopter') || (sprite.x < 174 && sprite.y < -120 && sprite.x > 154 && sprite.y > -140))[0];
    let xCoord = helicopter.x;
    let direction = helicopter.direction;
    let moved = false;
    let turned = false;
    t.addInput(0, {device: 'keyboard', key: 'space', isDown: true, duration: 500});
    t.addCallback(() => {
        if (helicopter.x > xCoord) {
            moved = true;
        }
        if (helicopter.direction < direction) {
            turned = true;
        }
    }, true);
    await t.runUntil(() => moved === true && turned === true, 5000);
    t.assert.ok(moved === true, 'helicopter must have moved');
    t.assert.ok(turned === true, 'helicopter must have turned');
    await t.runForSteps(200);
    t.assert.ok(helicopter.x === xCoord, 'helicopter must have returned to original spot');
    t.end();
};

const bowtieRotate = async function (t) {
    await t.runForSteps(50);
    let bowtie = t.getSprites(sprite => sprite.name.includes('Bowtie') || sprite.name.includes('bowtie') || (sprite.x < -80 && sprite.y < 30 && sprite.x > -91 && sprite.y > 19))[0];
    let bowtieDirection = bowtie.direction;
    let turned = false;
    t.addCallback(() => {
        if (bowtieDirection !== bowtie.direction) {
            turned = true;
        }
    }, true);
    let clickInput = {device: 'mouse', x: bowtie.x, y: bowtie.y, isDown: true, duration: 200};
    t.addInput(0, clickInput);
    await t.runForTime(500);
    t.assert.ok(turned === true, 'bowtie must have changed directionr');
    t.end();
};

const sunglassesColor = async function (t) {
    await t.runForSteps(50);
    let sunglasses = t.getSprites(sprite => sprite.name.includes('Sunglasses') || sprite.name.includes('sunglasses') || (sprite.x < -170 && sprite.y < 55 && sprite.x > -185 && sprite.y > 40))[0];
    let color = sunglasses.effects.color;
    let changedColor = false;
    t.addCallback(() => {
        if (color !== sunglasses.effects.color) {
            changedColor = true;
        }
    }, true);
    let clickInput = {device: 'mouse', x: sunglasses.x, y: sunglasses.y, isDown: true, duration: 200};
    t.addInput(0, clickInput);
    await t.runForTime(500);
    t.assert.ok(changedColor === true, 'sunglasses must have changed color');
    t.end();
};

const helicopterCostumeChange = async function (t) {
    let helicopter = await helicopterSetup(t);
    let costume = helicopter.currentCostume;
    let changedCostume = false;
    t.addCallback(() => {
        if (helicopter.currentCostume !== costume) {
            changedCostume = true;
        }
    }, true);
    await t.runUntil(() => changedCostume === true, 3000);
    t.assert.ok(changedCostume === true, 'costume must have changed');
    t.end();
};

const helicopterMoveUp = async function (t) {
    let helicopter = await helicopterSetup(t);
    let yCord = helicopter.y;
    let moved = false;
    t.addCallback(() => {
        if (helicopter.y > yCord) {
            moved = true;
        }
    }, true);
    t.addInput(0, {device: 'keyboard', key: 'up arrow', isDown: true, duration: 500})
    await t.runUntil(() => moved === true, 3000);
    t.assert.ok(moved === true, 'helicopter must have moved up');
    t.end();
};

const helicopterMoveDown = async function (t) {
    let helicopter = await helicopterSetup(t);
    let yCord = helicopter.y;
    let moved = false;
    t.addCallback(() => {
        if (helicopter.y < yCord) {
            moved = true;
        }
    }, true);
    t.addInput(0, {device: 'keyboard', key: 'down arrow', isDown: true, duration: 500})
    await t.runUntil(() => moved === true, 3000);
    t.assert.ok(moved === true, 'helicopter must have moved down');
    t.end();
};

const helicopterMoveLeft = async function (t) {
    let helicopter = await helicopterSetup(t);
    let xCord = helicopter.x;
    let moved = false;
    t.addCallback(() => {
        if (helicopter.x < xCord) {
            moved = true;
        }
    }, true);
    t.addInput(0, {device: 'keyboard', key: 'left arrow', isDown: true, duration: 500})
    await t.runUntil(() => moved === true, 3000);
    t.assert.ok(moved === true, 'helicopter must have moved left');
    t.end();
};

const helicopterMoveRight = async function (t) {
    let helicopter = await helicopterSetup(t);
    let xCord = helicopter.x;
    let moved = false;
    t.addCallback(() => {
        if (helicopter.x > xCord) {
            moved = true;
        }
    }, true);
    t.addInput(0, {device: 'keyboard', key: 'right arrow', isDown: true, duration: 500})
    await t.runUntil(() => moved === true, 3000);
    t.assert.ok(moved === true, 'helicopter must have moved right');
    t.end();
};

const helicopterSetup = async function (t) {
    await t.runForSteps(50);
    let helicopter = t.getSprites(sprite => sprite.name.includes('Helicopter') || sprite.name.includes('helicopter') || (sprite.x < 174 && sprite.y < -120 && sprite.x > 154 && sprite.y > -140))[0];
    let laptop = t.getSprites(sprite => sprite.name.includes('Laptop') || sprite.name.includes('laptop') || (sprite.x < 15 && sprite.y < -80 && sprite.x > 0 && sprite.y > -95))[0];
    let clickInput = {device: 'mouse', x: laptop.x, y: laptop.y, isDown: true, duration: 200};
    t.addInput(0, clickInput);
    return helicopter;
};

module.exports = [
    {
        test: devinInitialization,
        name: 'initialize Devin',
        description: '',
        categories: []
    },
    {
        test: alexInitialization,
        name: 'initialize Alex',
        description: '',
        categories: []
    },
    {
        test: rocksInitialization,
        name: 'initialize rocks',
        description: '',
        categories: []
    },
    {
        test: laptopInitialization,
        name: 'initialize laptop',
        description: '',
        categories: []
    },
    {
        test: antennaeInitialization,
        name: 'initialize antennae',
        description: '',
        categories: []
    },
    {
        test: sunglassesInitialization,
        name: 'initialize sunglasses',
        description: '',
        categories: []
    },
    {
        test: bowtieInitialization,
        name: 'initialize bowtie',
        description: '',
        categories: []
    },
    {
        test: helicopterInitialization,
        name: 'initialize helicopter',
        description: '',
        categories: []
    },
    {
        test: helicopterLooping,
        name: 'helicopter looping',
        description: '',
        categories: []
    },
    {
        test: bowtieRotate,
        name: 'bowtie rotating',
        description: '',
        categories: []
    },
    {
        test: sunglassesColor,
        name: 'sunglasses change color',
        description: '',
        categories: []
    },
    {
        test: helicopterCostumeChange,
        name: 'helicopter costume change',
        description: '',
        categories: []
    },
    {
        test: helicopterMoveUp,
        name: 'helicopter move up',
        description: '',
        categories: []
    },
    {
        test: helicopterMoveDown,
        name: 'helicopter move down',
        description: '',
        categories: []
    },
    {
        test: helicopterMoveLeft,
        name: 'helicopter move left',
        description: '',
        categories: []
    },
    {
        test: helicopterMoveRight,
        name: 'helicopter move right',
        description: '',
        categories: []
    }
];
