const arrowInitialization = async function (t) {
    let arrow = t.getSprites(sprite => sprite.name.includes('Arrow') || sprite.name.includes('arrow') || (sprite.x < 250 && sprite.y < 250 && sprite.x > -250 && sprite.y > -250))[0];
    t.assert.ok(arrow.visible, 'arrow must be visible');
    t.end();
};

const arrowMove = async function (t) {
    let arrow = t.getSprites(sprite => sprite.name.includes('Arrow') || sprite.name.includes('arrow') || (sprite.x < 250 && sprite.y < 250 && sprite.x > -250 && sprite.y > -250))[0];
    let moved = false;
    await t.runForSteps(2);
    let arrowX = arrow.x;
    let arrowY = arrow.y;
    t.onSpriteMoved(() => {
        if (!moved && arrowX !== arrow.x || arrowY !== arrow.y) {
            moved = true;
        }
    });
    await t.runUntil(() => moved === true, 3000);
    t.assert.ok(moved === true, 'arrow did not move');
    t.end();
};

const arrowMoveInsideBounds = async function (t) {
    let arrow = t.getSprites(sprite => sprite.name.includes('Arrow') || sprite.name.includes('arrow') || (sprite.x < 250 && sprite.y < 250 && sprite.x > -250 && sprite.y > -250))[0];
    let moved = true;
    await t.runForSteps(2);
    t.onSpriteMoved(() => {
        if (moved && arrow.y < -150 || arrow.y > 150 || arrow.x < -150 || arrow.y > 150) {
            moved = false;
        }
    });
    await t.runForTime(5000);
    t.assert.ok(moved === true, 'arrow did not stay inside bounds');
    t.end();
};

const arrowSizeIncrease = async function (t) {
    let arrow = t.getSprites(sprite => sprite.name.includes('Arrow') || sprite.name.includes('arrow') || (sprite.x < 250 && sprite.y < 250 && sprite.x > -250 && sprite.y > -250))[0];
    await t.runForSteps(10);
    let size = arrow.size;
    let decreased = false;
    t.addCallback(() => {
        if (!decreased && arrow.size < size) {
            decreased = true;
        }
    });
    t.addInput(0, {device: 'keyboard', key: 'space', isDown: true, duration: 10});
    await t.runForSteps(55);
    let sizeLittle = arrow.size;
    await t.runForSteps(100);
    t.assert.ok(decreased === true && sizeLittle < arrow.size, 'arrow did not increase size');
    t.end();
};

const arrowSizeDecrease = async function (t) {
    let arrow = t.getSprites(sprite => sprite.name.includes('Arrow') || sprite.name.includes('arrow') || (sprite.x < 250 && sprite.y < 250 && sprite.x > -250 && sprite.y > -250))[0];
    await t.runForSteps(10);
    let size = arrow.size;
    let decreased = false;
    t.addCallback(() => {
        if (!decreased && arrow.size < size) {
            decreased = true;
        }
    });
    t.addInput(500, {device: 'keyboard', key: 'space', isDown: true, duration: 500});
    await t.runUntil(() => decreased === true, 3000);
    t.assert.ok(decreased === true, 'arrow did not decrease size');
    t.end();
};

const arrowHitPoints = async function (t) {
    let arrow = t.getSprites(sprite => sprite.name.includes('Arrow') || sprite.name.includes('arrow') || (sprite.x < 250 && sprite.y < 250 && sprite.x > -250 && sprite.y > -250))[0];
    await t.runForSteps(10);
    let size = arrow.size;
    let decreased = false;
    t.addCallback(() => {
        if (!decreased && arrow.size < size) {
            decreased = true;
        }
    });
    t.addInput(0, {device: 'keyboard', key: 'space', isDown: true, duration: 500});
    await t.runForSteps(55);
    t.assert.ok(arrow.sayText.includes('points') === true, 'arrow did not say points');
    t.end();
};

module.exports = [
    {
        test: arrowInitialization,
        name: 'initialize arrow',
        description: '',
        categories: []
    },
    {
        test: arrowMove,
        name: 'moving arrow around',
        description: '',
        categories: []
    },
    {
        test: arrowMoveInsideBounds,
        name: 'moving arrow around inside borders',
        description: '',
        categories: []
    },
    {
        test: arrowSizeIncrease,
        name: 'arrow size increase',
        description: '',
        categories: []
    },
    {
        test: arrowSizeDecrease,
        name: 'arrow size decrease',
        description: '',
        categories: []
    },
    {
        test: arrowHitPoints,
        name: 'arrow hit',
        description: '',
        categories: []
    }
];
