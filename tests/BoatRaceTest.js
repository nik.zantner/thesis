const boatInitialization = async function (t) {
    let boat = t.getSprites(sprite => sprite.name.includes('Boat') || sprite.name.includes('boat') || (sprite.x < -120 && sprite.y < -120))[0];
    t.assert.ok(boat.visible, 'boat must be visible');
    t.end();
};

const gateInitialization = async function (t) {
    let gate = t.getSprites(sprite => sprite.name.includes('Gate') || sprite.name.includes('gate') || (sprite.x > 100 && sprite.y > 0))[0];
    t.assert.ok(gate.visible, 'gate must be visible');
    t.end();
};

const timeInitialization = async function (t) {
    const stage = t.getStage();
    let time = stage.getVariables(variable => variable.name.includes('time') || variable.name.includes('Time'))[0];
    t.assert.ok(time.value >= 0, 'time must be higher or equal to 0');
    t.end();
};

const timeIncrease = async function (t) {
    const stage = t.getStage();
    let time = stage.getVariables(variable => variable.name.includes('time') || variable.name.includes('Time'))[0];
    let startTime = time.value;
    await t.runForSteps(40);
    t.assert.ok(time.value > startTime, 'time must be higher than start time');
    t.end();
};

const spinGate = async function (t) {
    let gate = t.getSprites(sprite => sprite.name.includes('Gate') || sprite.name.includes('gate') || (sprite.x > 100 && sprite.y > 0))[0];
    let oldDirection = gate.direction;
    let directionChanged = false;
    t.addCallback(() => {
        if (!directionChanged && oldDirection < gate.direction) {

            directionChanged = true;
        }
    }, true);
    await t.runForTime(500);
    t.assert.ok(directionChanged === true, 'Gate did not turn right');
    t.end();
};

const boatMoveUp = async function (t) {
    let boat = t.getSprites(sprite => sprite.name.includes('Boat') || sprite.name.includes('boat') || (sprite.x < -120 && sprite.y < -120))[0];
    await t.runForSteps(5);
    let boatX = boat.x;
    let boatY = boat.y;

    let positionChanged = false;
    t.addCallback(() => {
        if (!positionChanged && (boatX !== boat.x || boatY !== boat.y)) {

            positionChanged = true;
        }
    }, true);
    await t.runForTime(500);
    t.assert.ok(positionChanged === true, 'Boat did not move');
    t.end();
};

const boatCrash = async function (t) {
    let boat = t.getSprites(sprite => sprite.name.includes('Boat') || sprite.name.includes('boat') || (sprite.x < -120 && sprite.y < -120))[0];
    await t.runForSteps(5);
    let boatX = boat.x;
    let boatY = boat.y;
    let touchedWall = false;
    t.onSpriteMoved(() => {
        if (!touchedWall && boat.isTouchingColor([102, 59, 0])) {
            touchedWall = true;
            boatX = boat.x;
            boatY = boat.y;
        }
    });
    await t.runUntil(() => touchedWall === true, 20000);
    t.assert.ok(touchedWall === true, 'Boat did not touch wall');
    await t.runForSteps(40);
    t.assert.ok(boat.x < boatX || boat.y < boatY, 'Boat did not reset');
    t.end();
};

const boatMoveRight = async function (t) {
    let boat = t.getSprites(sprite => sprite.name.includes('Boat') || sprite.name.includes('boat') || (sprite.x < -120 && sprite.y < -120))[0];
    await t.runForSteps(5);
    let boatX = boat.x;
    let moved = false;
    t.onSpriteMoved(() => {
        if (!moved && boat.x > boatX) {
            moved = true;
        }
    });
    let moveRightInput = {device: 'mouse', x: 250, y: boat.y, isDown: false, duration: 2000};
    t.addInput(0, moveRightInput);
    await t.runUntil(() => moved === true, 2000);
    t.assert.ok(moved === true, 'Boat did not move right');
    t.end();
};

const boatMoveLeft = async function (t) {
    let boat = t.getSprites(sprite => sprite.name.includes('Boat') || sprite.name.includes('boat') || (sprite.x < -120 && sprite.y < -120))[0];
    await t.runForSteps(5);
    let boatX = boat.x;
    let moved = false;
    t.onSpriteMoved(() => {
        if (!moved && boat.x < boatX) {
            moved = true;
        }
    });
    let moveLeftInput = {device: 'mouse', x: -250, y: boat.y, isDown: false, duration: 2000};
    t.addInput(0, moveLeftInput);
    await t.runUntil(() => moved === true, 2000);
    t.assert.ok(moved === true, 'Boat did not move left');
    t.end();
};

const boatMoveDown = async function (t) {
    let boat = t.getSprites(sprite => sprite.name.includes('Boat') || sprite.name.includes('boat') || (sprite.x < -120 && sprite.y < -120))[0];
    await t.runForSteps(5);
    let boatY = boat.y;
    let moved = false;
    t.onSpriteMoved(() => {
        if (!moved && boat.y < boatY) {
            moved = true;
        }
    });
    let moveDownInput = {device: 'mouse', x: boat.x, y: -250, isDown: false, duration: 2000};
    t.addInput(0, moveDownInput);
    await t.runUntil(() => moved === true, 2000);
    t.assert.ok(moved === true, 'Boat did not move down');
    t.end();
};

const boatTouchingArrow = async function (t) {
    let boat = t.getSprites(sprite => sprite.name.includes('Boat') || sprite.name.includes('boat') || (sprite.x < -120 && sprite.y < -120))[0];
    let touched = false;
    t.onSpriteMoved(() => {
        if (!touched && boat.isTouchingColor([255, 255, 255])) {
            touched = true;
        }
    });
    let moveDownInput = {device: 'mouse', x: -205, y: 8, isDown: false, duration: 5000};
    t.addInput(0, moveDownInput);
    await t.runUntil(() => touched === true, 5000);
    t.assert.ok(touched === true, 'Boat did not touch arrow');
    let boatY = boat.y;
    let boatX = boat.x;
    await t.runForSteps(1);
    let boatNewY = boat.y;
    let boatNewX = boat.x;
    let distance = Math.sqrt((boatNewX - boatX) * (boatNewX - boatX) + (boatNewY - boatY) * (boatNewY - boatY));
    t.assert.ok(distance >= 3, 'Boat did not move at least 3 ');
    t.end();
};

module.exports = [
    {
        test: boatInitialization,
        name: 'initialize boat',
        description: '',
        categories: []
    },
    {
        test: gateInitialization,
        name: 'initialize gate',
        description: '',
        categories: []
    },
    {
        test: timeInitialization,
        name: 'initialize time variable',
        description: '',
        categories: []
    },
    {
        test: timeIncrease,
        name: 'increasing time',
        description: '',
        categories: []
    },
    {
        test: spinGate,
        name: 'gate spinning',
        description: '',
        categories: []
    },
    {
        test: boatMoveUp,
        name: 'boat moving up',
        description: '',
        categories: []
    },
    {
        test: boatCrash,
        name: 'boat crashing',
        description: '',
        categories: []
    },
    {
        test: boatMoveRight,
        name: 'boat moving right',
        description: '',
        categories: []
    },
    {
        test: boatMoveLeft,
        name: 'boat moving left',
        description: '',
        categories: []
    },
    {
        test: boatMoveDown,
        name: 'boat moving down',
        description: '',
        categories: []
    },
    {
        test: boatTouchingArrow,
        name: 'boat touching arrow',
        description: '',
        categories: []
    }
];
