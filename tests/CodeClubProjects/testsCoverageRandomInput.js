const measureCoverage = async function (t) {
    const maxRuntimeWithNoChangeInSeconds = 60 * 1;
    const pastLogs = [];

    t.detectRandomInputs({duration: [100, 1000]});
    t.setRandomInputInterval(250);

    let currentSecond = 1;
    let isCoverage100Percent = false;
    let isCoverageUnchangedInLastMinute = false;

    while (!isCoverage100Percent && !isCoverageUnchangedInLastMinute) {
        await t.runUntil(() => t.getTotalRealTimeElapsed() >= 1000);
        const {covered, total} = t.getCoverage();

        t.log(`Covered ${covered} of ${total} after ${currentSecond} seconds`);

        isCoverage100Percent = covered / total === 1;

        isCoverageUnchangedInLastMinute = pastLogs[pastLogs.length - maxRuntimeWithNoChangeInSeconds]
            ? pastLogs[pastLogs.length - maxRuntimeWithNoChangeInSeconds].covered === covered
            : false;

        pastLogs.push({covered, atSecond: currentSecond});

        currentSecond++;
    }

    t.end();
};

module.exports = [
    {
        test: measureCoverage,
        name: 'Coverage Test',
        description: 'Run until 100% coverage has been reached by random input or no coverage change for one minute.',
        categories: []
    }
];
