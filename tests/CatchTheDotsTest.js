const controllerInitialization = async function (t) {
    let controller = t.getSprites(sprite => sprite.name.includes('Controller') || sprite.name.includes('controller') || (sprite.x < 10 && sprite.y < 10 && sprite.x > -10 && sprite.y > -10))[0];
    t.assert.ok(controller.visible, 'controller must be visible');
    t.end();
};

const blueInitialization = async function (t) {
    let blue = t.getSprites(sprite => sprite.name.includes('blue') || sprite.name.includes('Blue'))[0];
    t.assert.ok(!blue.visible, 'blue must not be visible');
    t.end();
};

const redInitialization = async function (t) {
    let red = t.getSprites(sprite => sprite.name.includes('red') || sprite.name.includes('Red'))[0];
    t.assert.ok(!red.visible, 'red must not be visible');
    t.end();
};

const yellowInitialization = async function (t) {
    let yellow = t.getSprites(sprite => sprite.name.includes('yellow') || sprite.name.includes('Yellow'))[0];
    t.assert.ok(!yellow.visible, 'yellow must not be visible');
    t.end();
};

const scoreInitialization = async function (t) {
    const stage = t.getStage();
    let score = stage.getVariables(variable => variable.name.includes('score') || variable.name.includes('Score'))[0];
    t.assert.ok(score.value === 0, 'Score must be 0');
    t.end();
};

const livesInitialization = async function (t) {
    const stage = t.getStage();
    let lives = stage.getVariables(variable => variable.name.includes('lives') || variable.name.includes('Lives'))[0];
    t.assert.ok(lives.value >= 0, 'Lives must be higher or equal to 0');
    t.end();
};

const spinRight = async function (t) {
    let sprite = t.getSprites(sprite => sprite.name.includes('Controller') || sprite.name.includes('controller') || (sprite.x < 10 && sprite.y < 10 && sprite.x > -10 && sprite.y > -10))[0];
    let oldDirection = sprite.direction;
    let directionChanged = false;
    t.addInput(0, {device: 'keyboard', key: 'right arrow', isDown: true, duration: 500});
    t.addCallback(() => {
        if (!directionChanged && oldDirection < sprite.direction) {

            directionChanged = true;
        }
    }, true);
    await t.runForTime(500);
    t.assert.ok(directionChanged === true, 'Sprite did not turn right');
    t.end();
};

const spinLeft = async function (t) {
    let sprite = t.getSprites(sprite => sprite.name.includes('Controller') || sprite.name.includes('controller') || (sprite.x < 10 && sprite.y < 10 && sprite.x > -10 && sprite.y > -10))[0];
    let oldDirection = sprite.direction;
    let directionChanged = false;
    t.addInput(0, {device: 'keyboard', key: 'left arrow', isDown: true, duration: 500});
    t.addCallback(() => {
        if (!directionChanged && oldDirection > sprite.direction) {

            directionChanged = true;
        }
    }, true);
    await t.runForTime(500);
    t.assert.ok(directionChanged === true, 'Sprite did not turn left');
    t.end();
};

const cloningRed = async function (t) {
    let red = t.getSprites(sprite => sprite.name.includes('red') || sprite.name.includes('Red'))[0];
    await cloneAndPosCheck(t, red, 2020);
};

const cloningYellow = async function (t) {
    let yellow = t.getSprites(sprite => sprite.name.includes('yellow') || sprite.name.includes('Yellow'))[0];
    await cloneAndPosCheck(t, yellow, 4020);
};

const cloningBlue = async function (t) {
    let blue = t.getSprites(sprite => sprite.name.includes('blue') || sprite.name.includes('Blue'))[0];
    await cloneAndPosCheck(t, blue, 6020);
};

const cloneAndPosCheck = async function (t, sprite, initialWaitingTime) {
    let cloneChecked = false;
    t.addCallback(() => {
        if (!cloneChecked && sprite.getClones().length !== 0) {
            let clone = sprite.getClones()[0];
            t.assert.ok((clone.x <= 180 && clone.x >= 179) || (clone.x >= -180 && clone.x <= -179), 'clone x coordinate is not near 180 or -180');
            t.assert.ok((clone.y <= 180 && clone.y >= 179) || (clone.y >= -180 && clone.y <= -179), 'clone y coordinate is not near 180 or -180');
            cloneChecked = true;
        }
    }, true);
    await t.runUntil(() => cloneChecked === true, initialWaitingTime);
    const clones = sprite.getClones();
    t.assert.ok(clones.length === 1, 'sprite not cloned exactly one time');
    t.end();
};

const movingRed = async function (t) {
    let red = t.getSprites(sprite => sprite.name.includes('red') || sprite.name.includes('Red'))[0];
    await movingCheck(t, red, 2020);
};

const movingYellow = async function (t) {
    let yellow = t.getSprites(sprite => sprite.name.includes('yellow') || sprite.name.includes('Yellow'))[0];
    await movingCheck(t, yellow, 4020);

};

const movingBlue = async function (t) {
    let blue = t.getSprites(sprite => sprite.name.includes('blue') || sprite.name.includes('Blue'))[0];
    await movingCheck(t, blue, 6020);
};

const movingCheck = async function (t, sprite, initialWaitTime) {
    const controller = t.getSprites(sprite => sprite.name.includes('Controller') || sprite.name.includes('controller') || (sprite.x < 10 && sprite.y < 10 && sprite.x > -10 && sprite.y > -10))[0];
    const contX = controller.x;
    const contY = controller.y;
    await t.runForTime(initialWaitTime);
    const clones = sprite.getClones();
    const spriteC = clones[0];
    const startX = spriteC.x;
    const startY = spriteC.y;
    const distanceStart = Math.sqrt((contX - startX) * (contX - startX) + (contY - startY) * (contY - startY));
    await t.runForTime(1000);
    const moveX = spriteC.x;
    const moveY = spriteC.y;
    const distanceMove = Math.sqrt((contX - moveX) * (contX - moveX) + (contY - moveY) * (contY - moveY));
    t.assert.ok(distanceMove < distanceStart, 'clone did not move to controller');
    t.end();
};

const scoringOrLosingRed = async function (t) {
    let red = t.getSprites(sprite => sprite.name.includes('red') || sprite.name.includes('Red'))[0];
    let redColor = [255, 0, 0];
    await testTouching(t, redColor, red, 2020);
};

const scoringOrLosingYellow = async function (t) {
    let yellow = t.getSprites(sprite => sprite.name.includes('yellow') || sprite.name.includes('Yellow'))[0];
    let yellowColor = [255, 255, 0];
    await testTouching(t, yellowColor, yellow, 4020);

};

const scoringOrLosingBlue = async function (t) {
    let blue = t.getSprites(sprite => sprite.name.includes('blue') || sprite.name.includes('Blue'))[0];
    let blueColor = [0, 63, 255];
    await testTouching(t, blueColor, blue, 6020)
};

const testTouching = async function (t, color, sprite, waitTime) {
    const controller = t.getSprites(sprite => sprite.name.includes('Controller') || sprite.name.includes('controller') || (sprite.x < 10 && sprite.y < 10 && sprite.x > -10 && sprite.y > -10))[0];
    const stage = t.getStage();
    await t.runForTime(waitTime);
    const clones = sprite.getClones();
    t.assert.ok(clones.length >= 1, 'not enough clones');
    const spriteClone = clones[0];
    let touchingController = false;
    let touchedControllerColor = false;
    const score = stage.getVariable('score').value;
    const lives = stage.getVariable('lives').value;
    t.onSpriteMoved(() => {
        if (!touchingController && controller.isTouchingSprite(sprite.name) === true) {
            touchingController = true;
        }
        if (!touchedControllerColor && sprite.isTouchingColor(color) === true) {
            touchedControllerColor = true;

        }
    });
    await t.runUntil(() => touchingController === true, 10000);
    await t.runForSteps(10);
    t.assert.ok(touchingController, 'controller was not touched');
    if (touchedControllerColor === true) {
        t.assert.ok(score < stage.getVariable('score').value, 'score was not increased');
    }
    t.assert.ok(stage.getVariable('lives').value <= lives, 'lives were increased');
    t.end();
};

module.exports = [
    {
        test: controllerInitialization,
        name: 'initialize controller',
        description: '',
        categories: []
    },
    {
        test: redInitialization,
        name: 'initialize red',
        description: '',
        categories: []
    },
    {
        test: blueInitialization,
        name: 'initialize blue',
        description: '',
        categories: []
    },
    {
        test: yellowInitialization,
        name: 'initialize yellow',
        description: '',
        categories: []
    },
    {
        test: livesInitialization,
        name: 'initialize lives variable',
        description: '',
        categories: []
    },
    {
        test: scoreInitialization,
        name: 'initialize score variable',
        description: '',
        categories: []
    },
    {
        test: spinRight,
        name: 'spin right',
        description: '',
        categories: []
    },
    {
        test: spinLeft,
        name: 'spin left',
        categories: []
    },
    {
        test: cloningRed,
        name: 'clone red',
        description: '',
        categories: []
    },
    {
        test: cloningYellow,
        name: 'clone yellow',
        description: '',
        categories: []
    },
    {
        test: cloningBlue,
        name: 'clone blue',
        description: '',
        categories: []
    },
    {
        test: movingRed,
        name: 'move red clone',
        description: '',
        categories: []
    },
    {
        test: movingYellow,
        name: 'move yellow clone',
        description: '',
        categories: []
    },
    {
        test: movingBlue,
        name: 'move blue clone',
        description: '',
        categories: []
    },
    {
        test: scoringOrLosingRed,
        name: 'red touches controller',
        description: '',
        categories: []
    },
    {
        test: scoringOrLosingYellow,
        name: 'yellow touches controller',
        description: '',
        categories: []
    },
    {
        test: scoringOrLosingBlue,
        name: 'blue touches controller',
        description: '',
        categories: []
    }
];
