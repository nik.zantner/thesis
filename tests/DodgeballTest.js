const picoInitialization = async function (t) {
    let pico = t.getSprites(sprite => (sprite.x < -100 && sprite.y < -100) || sprite.name.includes('walking') || sprite.name.includes('Walking'))[0];
    t.assert.ok(pico.visible, 'Pico must be visible');

    //  let trap = t.getSprite('Trapdoor');
    //   t.assert.ok(trap.visible, 'Trapdoor must be visible');
    t.end();
};

const laserInitialization = async function (t) {
    let laser = t.getSprites(sprite => sprite.name.includes('laser') || sprite.name.includes('Laser'))[0];
    t.assert.ok(laser.visible, 'Laser must be visible');
    t.end();
};


const ballInitialization = async function (t) {
    let ball = t.getSprites(sprite => sprite.name.includes('ball') || sprite.name.includes('Ball'))[0];
    t.assert.ok(!ball.visible, 'Ball must not be visible');
    t.end();
};


const jumpHeightInitialization = async function (t) {
    const stage = t.getStage();
    let jumpHeight = stage.getVariables(variable => variable.name.includes('Jump') || variable.name.includes('jump') || variable.name.includes('height') || variable.name.includes('Height'))[0];
    t.assert.ok(jumpHeight.value === 0, 'Jump height must be 0');
    t.end();
};

const gravityInitialization = async function (t) {
    const stage = t.getStage();
    let gravity = stage.getVariables(variable => variable.name.includes('gravity') || variable.name.includes('Gravity'))[0];
    t.assert.ok(gravity.value <= 0, 'Gravity must be lower or equal to 0');
    t.end();
};

const laserCostumeChange = async function (t) {
    let laser = t.getSprites(sprite => sprite.name.includes('laser') || sprite.name.includes('Laser'))[0];
    t.assert.ok((await costumeChanges(t, laser, 5000)) === true, 'Laser costume must have changed');
    t.end();
};


const trapCostumeChange = async function (t) {
    let trap = t.getSprite('Trapdoor');
    t.assert.ok((await costumeChanges(t, trap, 6000)) === true, 'Trapdoor costume must have changed');
    t.end();
};


const costumeChanges = async function (t, sprite, time) {
    let costume = sprite.currentCostume;
    let costumeChanged = false;
    t.onSpriteVisualChange(sprite => {
        costumeChanged = costumeChanged || costume !== sprite.currentCostume;
    });
    await t.runForTime(time);
    return costumeChanged;
};

const movePlayerLeft = async function (t) {
    await t.runForSteps(50);
    let pico = t.getSprites(sprite => (sprite.x < -100 && sprite.y < -100) || sprite.name.includes('walking') || sprite.name.includes('Walking'))[0];
    t.assert.ok((await moveTest(t, 'left arrow', pico)) === true, 'Player must move left');
    t.end();
};

const movePlayerRight = async function (t) {
    await t.runForSteps(50);
    let pico = t.getSprites(sprite => (sprite.x < -100 && sprite.y < -100) || sprite.name.includes('walking') || sprite.name.includes('Walking'))[0];
    t.assert.ok((await moveTest(t, 'right arrow', pico)) === true, 'Player must move right');
    t.end();
};

const moveTest = async function (t, dir, sprite) {
    await t.runForSteps(50);
    let oldX = sprite.x;
    let moved = false;
    t.addInput(0, {device: 'keyboard', key: dir, isDown: true, duration: 500});
    t.addCallback(() => {
        if (dir === 'left arrow' && sprite.x < oldX) {
            moved = true;
        } else if (dir === 'right arrow' && sprite.x > oldX) {
            moved = true;
        }
    }, true);
    await t.runForTime(500);
    return moved;
};

const jumpTest = async function (t) {
    await t.runForSteps(50);
    let pico = t.getSprites(sprite => (sprite.x < -100 && sprite.y < -100) || sprite.name.includes('walking') || sprite.name.includes('Walking'))[0];
    let oldY = pico.y;
    let jumped = false;
    t.addInput(0, {device: 'keyboard', key: 'space', isDown: true, duration: 500});
    t.addCallback(() => {
        if (pico.y > oldY) {
            jumped = true;
        }
    }, true);
    await t.runForTime(500);
    t.assert.ok(jumped === true, 'Player must jump up');
    await t.runForTime(1000);
    t.assert.ok(pico.isTouchingColor([0, 63, 255]), 'Player must land on ground');
    t.end();
};

const ladderTest = async function (t) {
    await t.runForSteps(50);
    let pico = t.getSprites(sprite => (sprite.x < -100 && sprite.y < -100) || sprite.name.includes('walking') || sprite.name.includes('Walking'))[0];
    let oldY = pico.y;
    let moveRightInput = {device: 'keyboard', key: 'right arrow', isDown: true, duration: 5000};
    t.addInput(0, moveRightInput);
    await t.runUntil(() => pico.isTouchingColor([255, 0, 191]) || pico.isTouchingColor([255, 255, 0]) === true, 5000);
    await t.runForSteps(5);
    t.inputImmediate({device: 'keyboard', key: 'right arrow', isDown: false});
    t.assert.ok(oldY === pico.y, 'Player must still be on ground');
    t.addInput(0, {device: 'keyboard', key: 'up arrow', isDown: true, duration: 3500});
    await t.runForTime(3500);
    t.assert.ok(oldY <= pico.y, 'Player must have climbed up');
    t.end();
};

const ballClone = async function (t) {
    let ball = t.getSprites(sprite => sprite.name.includes('ball') || sprite.name.includes('Ball'))[0];
    let cloned = false;
    t.addCallback(() => {
        if (!cloned && ball.getClones().length !== 0) {
            t.assert.ok(ball.getClones()[0].x > 110, 'Ball x must be upper right');
            t.assert.ok(ball.getClones()[0].y > 110, 'Ball y must be upper right');
            cloned = true;
        }
    }, true);
    await t.runUntil(() => cloned === true, 7000);
    t.assert.ok(cloned, 'Ball must have been cloned');
    t.end();
};

const ballMovingTopRow = async function (t) {
    let ball = t.getSprites(sprite => sprite.name.includes('ball') || sprite.name.includes('Ball'))[0];
    await t.runUntil(() => ball.getClones().length > 0, 10000);
    let ballC = ball.getClones()[0];
    let ballCX = ballC.x;
    let moved = false;
    t.addCallback(() => {
        if (!moved && ballC.x < ballCX) {
            moved = true;
        }
    }, true);
    await t.runUntil(() => moved, 2000);
    t.assert.ok(moved, 'Ball must  have moved left');
    t.end();
};

const ballMovingMiddleRow = async function (t) {
    let ball = t.getSprites(sprite => sprite.name.includes('ball') || sprite.name.includes('Ball'))[0];
    await t.runUntil(() => ball.getClones().length > 0, 10000);
    let ballC = ball.getClones()[0];
    await t.runUntil(() => ballC.isTouchingSprite('Trapdoor'), 10000);
    let ballCX = ballC.x;
    let moved = false;
    t.addCallback(() => {
        if (!moved && ballC.x > ballCX) {
            moved = true;
        }
    }, true);
    await t.runUntil(() => moved, 2000);
    t.assert.ok(moved, 'Ball must  have moved right');
    t.end();
};

const ballMovingBottomRow = async function (t) {
    let ball = t.getSprites(sprite => sprite.name.includes('ball') || sprite.name.includes('Ball'))[0];
    await t.runUntil(() => ball.getClones().length > 0, 10000);
    let ballC = ball.getClones()[0];
    let pico = t.getSprites(sprite => (sprite.x < -100 && sprite.y < -100) || sprite.name.includes('walking') || sprite.name.includes('Walking'))[0];
    await t.runUntil(() => ballC.y === pico.y, 20000);
    let ballCX = ballC.x;
    let moved = false;
    t.addCallback(() => {
        if (!moved && ballC.x < ballCX) {
            moved = true;
        }
    }, true);
    await t.runUntil(() => moved, 2000);
    t.assert.ok(moved, 'Ball must  have moved left');
    t.end();
};

const hitBallTest = async function (t) {
    let pico = t.getSprites(sprite => (sprite.x < -100 && sprite.y < -100) || sprite.name.includes('walking') || sprite.name.includes('Walking'))[0];
    let ball = t.getSprites(sprite => sprite.name.includes('ball') || sprite.name.includes('Ball'))[0];
    await t.runForSteps(50);
    let oldX = pico.x;
    let moveRightInput = {device: 'keyboard', key: 'right arrow', isDown: true, duration: 3000};
    t.addInput(0, moveRightInput);
    await t.runForTime(3000);
    t.assert.ok(oldX < pico.x, 'Player must have moved');
    let touchedBall = false;
    t.onSpriteMoved(() => {
        if (!touchedBall && pico.isTouchingSprite(ball.name)) {
            touchedBall = true;
        }
    });
    await t.runUntil(() => touchedBall === true, 20000);
    await t.runForSteps(5);
    t.assert.ok(oldX <= pico.x + 10 && oldX >= pico.x - 10, 'Player must have been reset');
    t.assert.ok(touchedBall === true, 'Ball was not touched');
    t.end();
};

const fallThroughTrapTest = async function (t) {
    let pico = t.getSprites(sprite => (sprite.x < -100 && sprite.y < -100) || sprite.name.includes('walking') || sprite.name.includes('Walking'))[0];
    let oldY = pico.y;
    let trap = t.getSprite('Trapdoor');
    const trapY = trap.y;
    let moveRightInput = {device: 'keyboard', key: 'right arrow', isDown: true, duration: 5000};
    t.addInput(0, moveRightInput);
    await t.runUntil(() => pico.isTouchingColor([255, 0, 191]) || pico.isTouchingColor([255, 255, 0]) === true, 5000);
    await t.runForSteps(5);
    t.inputImmediate({device: 'keyboard', key: 'right arrow', isDown: false});
    t.assert.ok(oldY === pico.y, 'Player must still be on ground');
    t.addInput(0, {device: 'keyboard', key: 'up arrow', isDown: true, duration: 1500});
    await t.runUntil(() => pico.y === trapY, 1500);
    await t.runForSteps(5);
    t.inputImmediate({device: 'keyboard', key: 'up arrow', isDown: false});
    t.assert.ok(oldY <= pico.y, 'Player must have climbed up');
    oldY = pico.y;
    t.addInput(0, {device: 'keyboard', key: 'left arrow', isDown: true, duration: 3500});
    await t.runUntil(() => pico.isTouchingSprite('Trapdoor'), 3500);
    await t.runForSteps(5);
    t.inputImmediate({device: 'keyboard', key: 'left arrow', isDown: false});
    await t.runForTime(3500);
    t.assert.ok(oldY >= pico.y, 'Player must have fallen down');
};

module.exports = [
    {
        test: picoInitialization,
        name: 'initialize pico',
        description: '',
        categories: []
    },
    {
        test: laserInitialization,
        name: 'initialize laser',
        description: '',
        categories: []
    },
    {
        test: ballInitialization,
        name: 'initialize ball',
        description: '',
        categories: []
    },
    {
        test: gravityInitialization,
        name: 'initialize gravity variables',
        description: '',
        categories: []
    },
    {
        test: jumpHeightInitialization,
        name: 'initialize jum height variables',
        description: '',
        categories: []
    },
    {
        test: laserCostumeChange,
        name: 'laser costumes change',
        description: '',
        categories: []
    }
    /*
    ,
    {
        test: trapCostumeChange,
        name: 'trapdoor costumes change',
        description: '',
        categories: []
    }
    */,

    {
        test: movePlayerLeft,
        name: 'player move left',
        description: '',
        categories: []
    },
    {
        test: movePlayerRight,
        name: 'player move right',
        description: '',
        categories: []
    },
    {
        test: jumpTest,
        name: 'player jump',
        description: '',
        categories: []
    },
    {
        test: ladderTest,
        name: 'climbing ladder',
        description: '',
        categories: []
    },
    {
        test: ballClone,
        name: 'ball clone',
        description: '',
        categories: []
    },
    {
        test: ballMovingTopRow,
        name: 'ball moving top',
        description: '',
        categories: []
    },
    {
        test: ballMovingMiddleRow,
        name: 'ball moving middle',
        description: '',
        categories: []
    },
    {
        test: ballMovingBottomRow,
        name: 'ball moving bottom',
        description: '',
        categories: []
    },
    {
        test: hitBallTest,
        name: 'player touches Ball',
        description: '',
        categories: []
    }
    /*
    ,
    {
        test: fallThroughTrapTest,
        name: 'player falls through trapdoor',
        description: '',
        categories: []
    }

     */
];
