\babel@toc {english}{}
\contentsline {chapter}{\nonumberline List of Tables}{III}{chapter*.12}% 
\contentsline {chapter}{\nonumberline List of Figures}{IV}{chapter*.14}% 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Introduction to Scratch}{1}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Introduction to Whisker}{2}{section.1.2}% 
\contentsline {section}{\numberline {1.3}Contributions}{4}{section.1.3}% 
\contentsline {section}{\numberline {1.4}Outline}{5}{section.1.4}% 
\contentsline {chapter}{\numberline {2}Related Work}{6}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Scratch Specific Testing and Execution}{6}{section.2.1}% 
\contentsline {subsection}{\numberline {2.1.1}ITCH}{6}{subsection.2.1.1}% 
\contentsline {subsection}{\numberline {2.1.2}Scratch Single Stepping}{6}{subsection.2.1.2}% 
\contentsline {section}{\numberline {2.2}Generic Test Execution}{7}{section.2.2}% 
\contentsline {chapter}{\numberline {3}Acceleration Options}{9}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Pre-Conditions}{9}{section.3.1}% 
\contentsline {section}{\numberline {3.2}Headless-mode}{9}{section.3.2}% 
\contentsline {subsection}{\numberline {3.2.1}Decoupling the Scratch VM from the renderer}{9}{subsection.3.2.1}% 
\contentsline {subsection}{\numberline {3.2.2}Running the Tests via Headless Chrome}{10}{subsection.3.2.2}% 
\contentsline {subsection}{\numberline {3.2.3}Not evaluated}{12}{subsection.3.2.3}% 
\contentsline {section}{\numberline {3.3}Modifying the update frequency of the Scratch VM}{12}{section.3.3}% 
\contentsline {subsection}{\numberline {3.3.1}Instrumenting primitives: \texttt {looks\_sayforsecs}, \texttt {looks\_thinkforsecs}, \texttt {looks\_glidesecstoxy} and \texttt {looks\_glideto}}{16}{subsection.3.3.1}% 
\contentsline {subsection}{\numberline {3.3.2}Instrumenting ioDevices: \texttt {clock.projectTimer}}{17}{subsection.3.3.2}% 
\contentsline {subsection}{\numberline {3.3.3}Modifying \texttt {getTotalTimeElapsed}, \texttt {getRunTimeElapsed}}{18}{subsection.3.3.3}% 
\contentsline {subsection}{\numberline {3.3.4}Benchmarking this instrumentation approach}{18}{subsection.3.3.4}% 
\contentsline {subsection}{\numberline {3.3.5}Threats to the frequency approach}{20}{subsection.3.3.5}% 
\contentsline {subsection}{\numberline {3.3.6}Why the test failure rate increases with high CPU load}{21}{subsection.3.3.6}% 
\contentsline {subsection}{\numberline {3.3.7}Alternative Approach: Instrumenting the Scratcht's Timer class}{22}{subsection.3.3.7}% 
\contentsline {subsection}{\numberline {3.3.8}Alternative Approach: Instrumenting JavaScript \texttt {Date.now}, \texttt {setTimeout} and \texttt {setInterval}}{22}{subsection.3.3.8}% 
\contentsline {section}{\numberline {3.4}Parallelizing test execution}{24}{section.3.4}% 
\contentsline {subsection}{\numberline {3.4.1}Parallelizing test execution in one Whisker-Web instance}{24}{subsection.3.4.1}% 
\contentsline {subsubsection}{\numberline {3.4.1.1}.. in one execution context}{24}{subsubsection.3.4.1.1}% 
\contentsline {subsubsection}{\numberline {3.4.1.2}.. in multiple execution contexts in one tab via WebWorkers}{25}{subsubsection.3.4.1.2}% 
\contentsline {subsection}{\numberline {3.4.2}Parallelizing test execution via multiple Whisker-Web instances}{25}{subsection.3.4.2}% 
\contentsline {chapter}{\numberline {4}Functional Enhancements}{29}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Changes to Whisker-Web}{29}{section.4.1}% 
\contentsline {subsection}{\numberline {4.1.1}Changes to the Interface}{29}{subsection.4.1.1}% 
\contentsline {subsection}{\numberline {4.1.2}The Servant}{29}{subsection.4.1.2}% 
\contentsline {subsection}{\numberline {4.1.3}General Usage}{29}{subsection.4.1.3}% 
\contentsline {subsection}{\numberline {4.1.4}Passing data between the Servant and Whisker-Web}{30}{subsection.4.1.4}% 
\contentsline {section}{\numberline {4.2}Changes to Whisker-Main}{30}{section.4.2}% 
\contentsline {subsection}{\numberline {4.2.1}Test Driver Enhancements}{30}{subsection.4.2.1}% 
\contentsline {subsection}{\numberline {4.2.2}Changes to the VMWrapper}{31}{subsection.4.2.2}% 
\contentsline {chapter}{\numberline {5}Evaluation}{32}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Evaluation Setup}{32}{section.5.1}% 
\contentsline {subsection}{\numberline {5.1.1}Applications, Metrics and Test Types}{32}{subsection.5.1.1}% 
\contentsline {subsection}{\numberline {5.1.2}Evaluation runs}{33}{subsection.5.1.2}% 
\contentsline {subsection}{\numberline {5.1.3}Platform}{35}{subsection.5.1.3}% 
\contentsline {section}{\numberline {5.2}Evaluation Results}{35}{section.5.2}% 
\contentsline {subsection}{\numberline {5.2.1}Integration-Test-Contraint (ITCon)}{35}{subsection.5.2.1}% 
\contentsline {subsection}{\numberline {5.2.2}Guided-Input-Constraint (GICon) Results}{36}{subsection.5.2.2}% 
\contentsline {subsection}{\numberline {5.2.3}Random-Input-Constraint (RICon) Results}{36}{subsection.5.2.3}% 
\contentsline {subsection}{\numberline {5.2.4}Random-Input-Coverage (RICov) Results}{37}{subsection.5.2.4}% 
\contentsline {subsection}{\numberline {5.2.5}Pearson Correlation}{39}{subsection.5.2.5}% 
\contentsline {section}{\numberline {5.3}Evaluation Conclusion}{40}{section.5.3}% 
\contentsline {chapter}{\numberline {6}Threats to the Approach}{41}{chapter.6}% 
\contentsline {chapter}{\numberline {7}Future Work and Recommendations}{43}{chapter.7}% 
\contentsline {section}{\numberline {7.1}Short Term Recommendations regarding Whisker}{43}{section.7.1}% 
\contentsline {section}{\numberline {7.2}Future Approaches for Whisker}{43}{section.7.2}% 
\contentsline {subsection}{\numberline {7.2.1}Interacting with Scratch}{43}{subsection.7.2.1}% 
\contentsline {subsection}{\numberline {7.2.2}Automatically finding stable acceleration options}{44}{subsection.7.2.2}% 
\contentsline {subsection}{\numberline {7.2.3}Test Prioritization}{44}{subsection.7.2.3}% 
\contentsline {chapter}{\numberline {8}Conclusion}{45}{chapter.8}% 
\contentsline {chapter}{\nonumberline Bibliography}{46}{chapter*.32}% 
