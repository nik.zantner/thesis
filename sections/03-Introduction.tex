\chapter{Introduction}
\pagenumbering{arabic}
\setcounter{page}{1}

As basic programming skills and understanding of computer programming are becoming more vital in the digitalized society, the need for tooling to teach and learn such skills grows \cite{Alves19}. A team at the Massachusetts Institute of Technology (MIT) started creating a visual programming language, currently available with a web-based integrated development environment (IDE) and a focus on usability. They named the language \textit{Scratch} \cite{Maloney04}. Scratch allows students or anyone interested in programming to develop simple games or applications, utilizing \textit{blocks} to represent controlling structures and variables, and so-called \textit{sprites} to represent user-interface (UI) elements \cite{Resnick09}.

Students starting to use Scratch to solve programming exercises will naturally need guidance and produce incorrect solutions for the tasks handed to them. A technical solution to automatically run Scratch applications against predefined tests, which evaluate the given application for specific constraints, would have several advantages. Firstly the work of the teacher or instructor correcting the applications could at least partially be automated. Secondly, while developing the applications, the students could benefit from validating their applications against such tests.

A test framework for Scratch applications seems desirable. \textit{Whisker} aims to be such a test framework \cite{StahlbauerKF19}. It allows users to define and test constraints based on sprite positions, interactions, timing, and other properties of the tested application. Also, input events can be triggered onto the Scratch canvas to replicate user interaction events. The concept is similar to integration tests, as software developers might write for any professional application. Several tests can be combined in one test file and represent a test-suite.

This thesis aims to improve Whisker by allowing its users to accelerate the execution of test-suites and more comfortably control the execution of several of those. Especially when running multiple test-suites, the results of the thesis save time, while not changing or distorting the test results.




\section{Introduction to Scratch}
The Lifelong Kindergarten group created Scratch at the MIT Media Lab \cite{ScratchHomePageFAQ} in 2007. The group describes their motivation with the following words:

\begin{quote}
  The Lifelong Kindergarten group develops new technologies and activities that, in the spirit of the blocks and finger paint of kindergarten, engage people in creative learning experiences. Our ultimate goal is to foster a world full of playfully creative people, who are constantly inventing new possibilities for themselves and their communities \cite{LifelongKindergarten}.
\end{quote}

The current Scratch version 3.0 was released in early 2019 and represented a complete reimplementation using only HTML5 and JavaScript while focusing on an enhanced, touch-optimized user interface \cite{ScratchWikiThreePointZero}. The following figure shows the Scratch online IDE with an exemplary application loaded. The code is represented by the blocks in the center, the application preview is to the right, and the list of available blocks to the left.

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\linewidth]{./figures/scratch_screenshot.png}
  \caption{The Scratch editor and preview used to create Scratch programs available at \url{scratch.mit.edu}.}
\end{figure}

The Scratch project is developed as an open-source application and can be found on Github \cite{ScratchSource}. There are several separate repositories, for example the \textit{scratch-gui}, the \textit{scratch-renderer} and the \textit{scratch-vm}. For this thesis, the \textit{scratch-vm} represents the point of focus, as this repository includes the logic used to run Scratch applications. The Scratch Virtual Machine is not a Virtual Machine (VM) to imitate computation hardware or peripherals as KVM or VirtualBox, but a VM as developers know it from the Java Virtual Machine. It interprets, executes, and manages the state of programs defined via the Scratch programming language.




\section{Introduction to Whisker}
Whisker is a testing utility for Scratch applications, enabling developers to write automated tests for Scratch applications, which run the application, assess its state with predefined constraints, and trigger input events onto the Scratch application UI. The Whisker authors describe their motivation with the following words:

\begin{quote}
  The main goal [...] is to be able to automatically assess student’s solutions to Scratch assignments. To do so, this approach makes use of an automation utility, which allows test code to interact with Scratch programs through Scratch’s IO \cite{StahlbauerKF19}.
\end{quote}

Whisker is being developed by the Chair of Software Engineering II at the University Passau and is available as an open-source application at Github \cite{WhiskerSource}. Exactly as Scratch, it is written in JavaScript and currently consists of two repositories, \textit{whisker-web} and \textit{whisker-main}. Whisker-Web includes the code-base to start Whisker in a web browser, allowing the user to select a Scratch application, a test-suite file, run the tests, watch the application run and display the output as TAP13 formatted test report. The tests can also be edited on the fly and executed individually \cite{TestAnything}.

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\linewidth]{./figures/whisker_screenshot.png}
  \caption{The Whisker-Web frontend with a loaded Scratch application named \textit{CatchGame.sb3} and test file named \textit{CatchGameTest.js}.}
\end{figure}

Whisker-Main contains the actual logic to wrap and control the underlying Scratch VM, which is included as an NPM module. It also exports a test-driver object, representing the API test developers can use to interact with and assess the state of the tested Scratch application.

The following example showcases the functionality of Whisker. The tested Scratch application is displayed in the two previous figures and is from now on referred to as the \textit{Catch Game}. It consists of a bowl at the bottom, which is moved by the player to the left and right by the corresponding arrow keys. Bananas and apples fall from the top of the screen and should be caught by the player with the bowl. Those three objects represent the available sprites defined in the Scratch application source code. Each caught apple results in 5 points, and each caught banana results in 8 points. The game ends either after a maximum of 30 seconds has passed, or an apple touches the red line at the bottom, which ends the game immediately. The overall goal is to achieve as many points as possible by catching as many fruits as possible in 30 seconds.

An exemplary test-suite for the Catch Game consists of 28 tests. As the test file consists of more than 1000 lines, only an excerpt will be shown. The following test is named \textit{Fruit Initialization Test} and will be closely examined. The description of the test reads \flqq Tests the initialization of the size of apples and bananas. Apples and bananas must both have a size of 50\%.\frqq{} and consists of the following code:

\begin{lstlisting}[language=ES6]
const testFruitInitialization = async function (t) {
  const {apple, banana} = getSpritesAndVariables(t, ['apple', 'banana']);

  // Give the program some time to initialize.
  await t.runUntil(() => apple.size === 50 && banana.size === 50, 500);

  t.assert.equal(apple.size, 50, 'Apple must have a size of 50%.');
  t.assert.equal(banana.size, 50, 'Banana must have a size of 50%.');

  t.end();
};
\end{lstlisting}
The test gets the loaded apple and banana sprite objects, runs the Scratch application until the size of the sprites has been reduced by 50 \% or 500 ms have passed and asserts the size of the banana and apple sprites.

Running the test-suite yields a coverage and test-failure report when the run is finished. Such a report might look like this:
\begin{lstlisting}[language=ES6]
  # summary:
  #   tests: 28
  #   pass: 28
  #   fail: 0
  #   error: 0
  #   skip: 0
  # coverage:
  #   combined: 1.00 (54/54)
  #   individual:
  #     Stage: 1.00 (6/6)
  #     Bowl: 1.00 (10/10)
  #     Apple: 1.00 (15/15)
  #     Bananas: 1.00 (23/23)
\end{lstlisting}
The report reveals, that the tested application passes all tests and a coverage of a 100 \% of all statements or blocks in Scratch have been achieved.




\section{Contributions}
The main contribution of this thesis is to allow Whisker users to accelerate the execution of a test-suite. Acceleration is achieved by parallel execution and the acceleration of sequentially running tests. Additionally, the tests can now be executed \textit{headless}, allowing users to run tests on headless environments like AWS instances and their local machines without interrupting other work. Those new options can be controlled via a command-line utility, which is called the \textit{Servant} as in \textit{"Cats don't have owners they have servants"}. The Servant is part of the Whisker-Web repository and lives in the \texttt{./servant} folder. It is written in JavaScript as Whisker and Scratch are and can be executed as a Node.js application. More technical insights can be found in the chapter \textit{The Servant}.

The Servant and the different acceleration options have been evaluated with real-world Scratch applications from different educational workshops and courses. The thesis yields the following results:
\begin{enumerate}
  \item Acceleration can be achieved by increasing the frequency of the Scratch VM's internal state update interval, without increasing the number of failing tests compared to the default frequency. How much the frequency can be increased depends on the actual hardware being used. On an Intel Core i5-8250U processor, a tenfold increase yields accurate and reliable test results.

  \item Acceleration can be achieved by distributing test-suite tests over multiple Whisker instances and executing those instances in parallel. The amount of parallel instances is limited by the number of threads or CPUs available, otherwise the test-failure rate increases. Test result reporting can also be achieved with parallel execution, changes to the code-base to allow merging coverage, and test result reports have been added.

  \item Both acceleration options can be controlled by the Whisker user and do not required changes in any of the Scratch source code repositories.

  \item A command-line interface allows execution in headless environments and from other scrips.

  \item New functions have been added to the test driver, allowing to measure real-time and get the current coverage.
\end{enumerate}




\section{Outline}
This chapter started with a top-level introduction to Scratch and Whisker, showcasing the functionality of both without going deeper into their respective code-bases. The previous section included the contributions made by this thesis.

The second chapter showcases some related work, in the context of Scratch application testing and acceleration.

The third chapter contains a detailed analysis of how acceleration can be achieved in different manners, beginning with a look into a headless execution to reduce the rendering needs. How the sequential acceleration based on the Scratch VM frequency can be achieved follows in the next section, while the last section describes parallel execution.

The fourth chapter describes the changes made to the Whisker code-base, split into the newly added Servant, and the changes made to the Whisker Web and Whisker Main repository in their respective sections.

The fifth chapter holds the evaluation design and evaluation results. The evaluation shows that a measurable acceleration in terms of a reduced execution duration is achieved, coverage generation is produced quicker, and there is no difference in test results between accelerated and non-accelerated execution.

The sixth chapter lists threats to the chosen approach and the validity of the thesis.

The seventh and last chapter gives several recommendations for the future Whisker development, split into a short term and long term view.
