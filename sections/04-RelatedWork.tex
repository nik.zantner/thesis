\chapter{Related Work}


\section{Scratch Specific Testing and Execution}

\subsection{ITCH}
At the time of writing, the only other project enabling non-static testing of Scratch applications publicly available is \textit{ITCH}. The paper \textit{ITCH: Individual Testing of Computer Homework for Scratch Assignments} \cite{Johnson16} introduced a utility to automize student assignment testing of Scratch 2 applications.

ITCH consists of a collection of python scripts that modify the original Scratch application by replacing input event triggers with the \textit{green-flag-pressed} start instruction and output instructions with the \textit{write-to-variable} instruction.
\begin{figure}[H]
  \centering
  \includegraphics[width=0.7\linewidth]{./figures/ITCH_screenshot.png}
  \caption{Excerpt from the ITCH paper, the original caption reads:
    \textit{On the left is a small script to count from 1 to 10. On the right is the ITCH-augmented script with a replaced start event block, an added informational comment block, and a transformation from a text-bubble “say” block to storage in the ITCH “output” list}
    \cite{Johnson16}.
  }
\end{figure}
The modified application is automatically loaded in the Scratch online IDE via Apple’s Mac OS X Automator and executed in a non-modified Scratch environment. After the execution is completed, the Automator downloads the Scratch application, and the results of the test run are extracted. Only the Scratch application is modified to allow event triggering and information extraction.

As the execution of the Scratch application itself is not modified, no acceleration is performed.


\subsection{Scratch Single Stepping}
In Scratch 2 applications, the execution speed of those could be modified in the official Scratch editor itself, to enable step-by-step debugging. This feature was called \textit{Single Stepping} \cite{ScratchWikiSingleStepping}. Additionally, the \textit{Turbo Speed} option was introduced to enable as-fast-as-possible execution. In Scratch 3 applications only the Turbo Speed option is available, now renamed to \textit{Turbo Mode (3.0)} \cite{ScratchWikiTurboMode}. Applications executed in turbo mode do not correctly update their UI representation but only the underlying logic. Also, no block containing a time value as control-variable, like \textit{wait-for-x-seconds} is modified, therefore those blocks are not accelerated. The turbo mode is not a generic solution to accelerate the execution but to be used in particular use-cases, or as the Scratch Wiki entry formulates it \cite{ScratchWikiTurboMode}:
\begin{quote}
  Projects that might use turbo mode include, but are not limited to:
  \begin{itemize}
    \item Drawing projects
    \item Big mathematical operations
    \item 3D engines
    \item Pen projects
  \end{itemize}
\end{quote}

Currently, no general-purpose execution acceleration for Scratch applications exists, neither in the context of Scratch application testing or generic execution.




\section{Generic Test Execution}

As testing is a standard part of every software engineering process, several generic techniques to reduce test run times, and therefore costs, have been proposed in the past decades.

Saff et al. suggest using \textit{mock objects} to accelerate test execution:
\begin{quote}
  Mock objects, like stubs, simulate an expensive resource, but mock objects also assert that they are used in a specific way [...]. If the simulation is faithful to the expensive resource, then a test that utilizes the mock object rather than the expensive resource can be cheaper (for example, faster) \cite{Saff05}.
\end{quote}
This approach can work well, under the condition that \textit{expensive resources} exist. In Whisker, those resources are no bottlenecks, as the first section in the chapter \textit{Analysis} shows. Therefore this approach was not followed up on in this thesis.

Elbaum et al. propose an improved test prioritization for regression testing with different goals. One of their goals is defined as \flqq Testers may wish to increase the rate of fault detection of test suites - that is, the likelihood of revealing faults earlier in a run of regression tests using those suites\frqq{} \cite{Elbaum00}. This goal can be defined as a test acceleration, in terms of failing faster and ending the test run at the first discovered failure.
\begin{quote}
  [...] given program P and test suite T, we prioritize the test cases in T with the intent of finding an ordering that will be useful on a specific version P0 of P. Version-specific prioritization is performed after a set of changes have been made to P and prior to regression testing P0. The prioritized test suite may be more effective at meeting the goal of the prioritization for P0 in particular than would a test suite resulting from general test case prioritization [...] \cite{Elbaum00}.
\end{quote}
Such an improved prioritization could be implemented within Whisker by recording for each test in a test-suite, how long it takes, and what blocks and sprites are covered by it. When a new version of the Scratch application is loaded, Whisker could detect changed blocks and sprites which already existed within the last test run and prioritize the test runs covering those blocks. For this thesis, this approach was not used, as it is specific for one use case of Whisker, but does not allow any generic acceleration.

Avoiding testing logic twice, referred to as \textit{coverage overlap optimization} is another generic method to reduce test run duration. Several different strategies exist, also in combination with prioritization or re-ordering of tests \cite{Bach17, Agrawal19, Vahabzadeh18}. On the other hand, a study by Rothermel et al. also identified threats implied by minification approaches:
\begin{quote}
  Our results strongly support our first three hypotheses: given test suites that are coverage-adequate and coverage-redundant, test suite minimization can provide significant savings in test suite size. These savings can increase as the size of the original test suites increases, and these savings are relatively highly correlated (logarithmically) with test suite size. However, minimization can significantly compromise the fault-detection abilities of such test suites, even when the test suites are not particularly large \cite{Rothermel98}.
\end{quote}
Still, the authors emphasize that \flqq several threats to external validity that limit our ability to generalize our results;\frqq{} exist. None of the strategies was used to avoid issues concerning test minification, prioritization, or any modification.

To summarize, changing the actual test-logic defined in the test-suite executed in Whisker or applying any acceleration only for a specific goal like increasing coverage was beyond this thesis's scope. This is based on the findings explained in the \textit{Analysis} chapter, showing that the pre-thesis state of Whisker did not utilize the available computing resources of the host computer. Removing these bottlenecks as it improves every test-suite was identified as the target of this thesis.
