\chapter{Evaluation}


\section{Evaluation Setup}

\subsection{Applications, Metrics and Test Types}
The results of this thesis are evaluated on a collection of existing test-suites and Scratch applications. The questions the evaluation should answer are defined as follows:
\begin{itemize}
  \item RQ1: How do an increased update frequency and parallelization influence test execution duration?
  \item RQ2: How do an increased update frequency and parallelization influence the test-failure rate?
  \item RQ3: How does an increased update frequency influence the test coverage?
\end{itemize}

The test-suites used for the evaluation are categorized into different types. The first type \textit{Integration-Test-Contraints (ITCon)} describes a test-suite that tests the behavior of a specific Scratch application, based on assertions and event triggers as done in common integration tests. The second type \textit{Guided-Input-Constraint (GICon)} is also Scratch application-specific. It tries to play a Scratch application in such a manner that it wins the game while testing for application-specific constraints. The third type \textit{Random-Input-Constraint (RICon)} does the same as the GICon test, with random input instead of guided input. Playing the game in this way does not ensure winning the game. The fourth type \textit{Random-Input-Coverage (RICov)} tries to achieve high coverage of the Scratch application's statements, but without application-specific tests, only by using random inputs.

The metrics examined are the acceleration meaning \textit{How much can the test execution duration be reduced?} without increasing the flakiness of the tests, therefore also asking \textit{How does the test failure-rate differ, when accelerating the tests?}. Those questions refer to RQ1 and RQ2. Additionally, in the context of test-suites targeting the coverage of a Scratch application, the test coverage differences between accelerated and non-acceleration runs are evaluated. This results in the question \textit{How does the test coverage differ when accelerating the tests?}, referring to RQ3.

To summarize, the evaluation is based on the interaction between test execution acceleration and test result reliability in terms of flakiness (test types ending in \textit{Con}) or coverage variation (test types ending in \textit{Cov}).

The following table shows which Scratch projects are evaluated by what kind of test-suite type:
\begin{table}[H]
  \begin{tabular}{lrrrr}
  Scratch Project Name  & ITCon test       & GICon test    & RICon test    & RICov test         \\ \hline
  Catch Game            & \checkmark       & \checkmark    & \checkmark    & \checkmark         \\
  Archery               & \checkmark       &               &               & \checkmark         \\
  Balloons              &                  &               &               & \checkmark         \\
  Beat The Goalie       &                  &               &               & \checkmark         \\
  Boat Race             & \checkmark       &               &               & \checkmark         \\
  Brain Game            &                  &               &               & \checkmark         \\
  Catch The Dots        & \checkmark       &               &               & \checkmark         \\
  Chat Bot              &                  &               &               & \checkmark         \\
  Clone Wars            &                  &               &               & \checkmark         \\
  Create Your Own World &                  &               &               & \checkmark         \\
  Dodgeball             & \checkmark       &               &               & \checkmark         \\
  Ghostbusters          &                  &               &               & \checkmark         \\
  Green Your City       & \checkmark       &               &               & \checkmark         \\
  Lost In Space         &                  &               &               & \checkmark         \\
  Memory                &                  &               &               & \checkmark         \\
  Moonhack Scratch 2017 &                  &               &               & \checkmark         \\
  Poetry Generator      &                  &               &               & \checkmark         \\
  Rock Band             &                  &               &               & \checkmark         \\
  Snowball Fight        &                  &               &               & \checkmark         \\
  Space Junk            &                  &               &               & \checkmark         \\
  Sprint                &                  &               &               & \checkmark         \\
  Synchronised Swimming &                  &               &               & \checkmark         \\
  Tech Toys             & \checkmark       &               &               & \checkmark         \\
  Username Generator    &                  &               &               & \checkmark         \\
  ZPaintBox             &                  &               &               & \checkmark
  \end{tabular}
  \caption{This table shows which application was evaluated under which type of test-suite.}
\end{table}


\subsection{Evaluation runs}
Every \checkmark represents an evaluation run. Each evaluation run uses another combination of acceleration options (parallelization and frequency). Each of the combinations of options is not only run once, but 30 times to reduce the effect of statistical outliers. For one \textit{ITCon} evaluation, the following list shows the used acceleration options:
\begin{itemize}
  \item acceleration factors of 1 (no acceleration), no headless (defines the baseline other runs are compared to)
  \item acceleration factors of 5, 10, 20, 40 no headless
  \item acceleration factors of 1, 5, 10, 20, 40, each headless
  \item acceleration factors of 1, 5, 10, 20, 40, each headless, each two tabs
  \item acceleration factors of 1, 5, 10, 20, 40, each headless, each four tabs
  \item acceleration factors of 1, 5, 10, 20, 40, each headless, each eight tabs
\end{itemize}
More than an acceleration factor of 40 and eight tabs have not been used, as the browser tabs often crash, and therefore, most tests fail.

For \textit{GICon}, \textit{RICon} and \textit{RICov} runs tab-based parallelization is not used. This is because those test-suites only consist of a single test, therefore not allowing parallelization:
\begin{itemize}
  \item acceleration factors of 1 (no acceleration), no headless (defines our baseline)
  \item acceleration factors of 5, 10, 20, 40 no headless
  \item acceleration factors of 1, 5, 10, 20, 40, each headless
\end{itemize}

As the test-suite for the \textit{RICov} evaluation run is generic to all the tested applications, the evaluation code will be explained:
\begin{lstlisting}[language=ES6]
const measureCoverage = async function (t) {
  const maxRuntimeWithNoChangeInSeconds = 60;
  const pastLogs = [];

  t.detectRandomInputs({duration: [100, 1000]});
  t.setRandomInputInterval(250);

  let currentSecond = 1;
  let isCoverage100Percent = false;
  let isCoverageUnchangedInLastMinute = false;

  while (!isCoverage100Percent && !isCoverageUnchangedInLastMinute) {
      await t.runUntil(() => t.getTotalRealTimeElapsed() >= 1000);
      const {covered, total} = t.getCoverage();

      t.log(`Covered ${covered} of ${total} after ${currentSecond} seconds`);

      isCoverage100Percent = covered / total === 1;

      const logToCompareWith = pastLogs[pastLogs.length - maxRuntimeWithNoChangeInSeconds];
      isCoverageUnchangedInLastMinute = logToCompareWith
        ? logToCompareWith.covered === covered
        : false;

      pastLogs.push({covered, atSecond: currentSecond});

      currentSecond++;
  }

  t.end();
};
\end{lstlisting}

The code executes random inputs on the Scratch application, either until the coverage reaches a 100 \% or the coverage did not change for 1 minute (see the \texttt{maxRuntimeWithNoChangeInSeconds} variable). Those conditions are evaluated after running the application for a second of real-time using a new test driver function (see the \texttt{getTotalRealTimeElapsed} function call). This function returns the actual time that passed since the application was started, not incorporating the acceleration factor.

To showcase this with an example, using a stopwatch to measure realtime \texttt{getTotalRealTimeElapsed} will return 10 seconds if the application is stopped after 10 seconds have passed on a stopwatch, independent of the used acceleration factor. If an acceleration factor of 10 is used, the already existing test driver method \texttt{getRunTimeElapsed} will, on the other hand, only return 1 second, as the application is running ten times faster.


\subsection{Platform}
The platform the evaluation was executed on was a Lenovo t480s with following hardware properties:

\begin{table}[H]
  \begin{tabular}{ll}
  CPU     & i5-8250U             \\
  GPU     & UHD Graphics 620     \\
  RAM     & 16 GB DDR4           \\
  OS      & Arch Linux           \\
  Kernel  & Linux 5.5.8-1-clear  \\
  Browser & Chromium 79.0.3942.0
  \end{tabular}
  \caption{This table shows the hardware used for the evaluation.}
\end{table}




\section{Evaluation Results}


\subsection{Integration-Test-Contraint (ITCon)}
The following chart shows how the used acceleration options affect the execution duration and test failure count for seven applications under Integration-Test-Contraint (ITCon) evaluation. Thirty runs have been used for the selected acceleration option combinations resulting in 5250 data rows in total. The duration has been normalized for each application to generate comparable values.

On the x-Axis, the \textit{-a} value represents the used acceleration factor and \textit{-d} if the headless mode has been used. The default of Whisker is \textit{-a 1}. On the left y-Axis, the mean of the absolute failures is plotted. On the right y-Axis, the execution duration in percent as a boxplot is plotted. A 100 \% represents the longest measured duration. Less execution duration and fewer failures are better.
\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\linewidth]{./figures/evaluation_ITCon_normalized.png}
  \caption{
    Integration-Test-Contraint (ITCon) results for seven applications, showing the number of test failures (barplot) and execution time (boxplot) for different frequencies and parallelization levels.
  }
\end{figure}
The first boxplot element denoted on the x-Axis with a \textit{-a 1} represents our default mode in Whisker - no parallelization and no frequency-based acceleration. The following x-Axis elements represent the runs at an acceleration factor of 5, 10, 20, and 40 with no parallelization. The decreasing boxplot reflects the decreasing execution duration, while the barplot shows test failures with an inreasing acceleration factor. The following groups of five x-Axis elements represent the same frequency ramp repeated while increasing the parallelization level from 1 over 2, 4 to 8. As the bar plots show, with an increased parallelization level, the number of test failures also increases, also with acceleration factors lower than 40. Still, the execution duration drops as the boxplots show.

Within the limits of the available CPU power, the acceleration factor of 10 without parallelization produces reliable test results and decreases the execution duration by roughly 80 \%.


\subsection{Guided-Input-Constraint (GICon) Results}
The following chart shows how the used acceleration options affect the execution duration and test failure count for the Catch Game under Guided-Input-Constraint (GICon) evaluation. Thirty runs have been used for the selected acceleration option combinations resulting in 300 data rows in total.

On the x-Axis, the \textit{-a} value represents the used acceleration factor and \textit{-d} if the headless mode has been used. The default is \textit{-a 1}. On the left y-Axis, the absolute failures are plotted. For all options, the number of failed tests was 0. On the right y-Axis, the execution duration in seconds as a boxplot is plotted. Less execution duration and fewer failures are better.
\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\linewidth]{./figures/evaluation_GICon.png}
  \caption{
    Guided-Input-Constraint (GICon) Results for the Catch Game, showing the number of test failures (barplot) and execution time (boxplot) for different acceleration factors and parallelization levels.
  }
\end{figure}


\subsection{Random-Input-Constraint (RICon) Results}
The following chart shows how the used acceleration options affect the execution duration and test failure count for the Catch Game under Random-Input-Constraint (RICon) evaluation. Thirty runs have been used for the selected acceleration option combinations resulting in 300 data rows in total.

On the x-Axis, the \textit{-a} value represents the used acceleration factor and \textit{-d} if the headless mode has been used. The default is \textit{-a 1}. On the left y-Axis, the absolute failures are plotted (for all options, this value is 0). On the right y-Axis, the execution duration in seconds as a boxplot is plotted. Less execution duration and fewer failures are better.
\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\linewidth]{./figures/evaluation_RICon.png}
  \caption{
    Random-Input-Constraint (RICon) Results for the Catch Game, showing the number of test failures (barplot) and execution time (boxplot) for different frequencies and parallelization levels.
  }
\end{figure}


\subsection{Random-Input-Coverage (RICov) Results}
The following combination of boxplot and barplot shows how the used acceleration options affect the execution duration and test coverage for 25 applications under Random-Input-Coverage (RICov) evaluation. Thirty runs have been used once with an acceleration factor of 1 (timeout after 600 seconds) and once with an acceleration factor of 10 (timeout after 120 seconds). In both cases, the run also ended after 100 \% coverage had been reached.

The x-Axis lists the application names and two boxplots and bar plots for each application. Blue represents an acceleration factor of 1, orange an acceleration factor of 10 for both plots. The left y-Axis represents the execution duration in seconds as a boxplot of the thirty runs. The right y-Axis represents the coverage as a barplot from 0 to 100 \%, using the mean of the thirty runs. Less execution duration and more coverage are better.
\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\linewidth]{./figures/evaluation_RICov.png}
  \caption{
    Random-Input-Coverage (RICov) results from 25 applications, showing the coverage in percent (barplot) and execution time (boxplot) for each application. Orange represents the results for an acceleration factor of 10 and blue for an acceleration factor of 1.
  }
\end{figure}
The generated coverage performs only scarcely some single-digit percent better or worse at an acceleration factor of 10 than with an acceleration factor of 1. At the same time, the execution duration is always reduced, the coverage generation therefore accelerated.

As the last chart is very dense with information, the next collection of charts will show the same information for each of the 25 applications. Again, thirty runs have been used once with an acceleration factor of 1 and once with an acceleration factor of 10, ending after the coverage has not changed for 60 seconds in realtime or 100 \% coverage reached.

Each chart plots the coverage change for one application, blue again representing the acceleration factor of 1 and orange the acceleration factor of 10. The y-Axis represents the coverage in \%. The x-Axis represents the execution duration in seconds. Less execution duration and more coverage are better.
\begin{figure}[H]
  \centering
  \includegraphics[width=1\linewidth]{./figures/evaluation_RICov_per_app_2_split_2.png}
\end{figure}
\begin{figure}[H]
  \centering
  \includegraphics[width=1\linewidth]{./figures/evaluation_RICov_per_app_2_split_1.png}
  \caption{
    Random-Input-Coverage (RICov) results from 25 applications, showing the coverage in percent on the line plot's y-Axis and execution time on the x-Axis for each application. Orange represents the results for an acceleration factor of 10 and blue for an acceleration factor of 1. In the chart for the Rockband.sb3 application, the two plots show the same results. Therefore only the result of an acceleration factor of 10 in the foreground is visible.
  }
\end{figure}

Some of the charts do not display a static trend of coverage increase but have short, temporary downward movements. For example the last chart for the \texttt{ZPaintBox.sb3} displays such a short downward movement at second 50 of the blue plot. This occurs, as the data was gathered over 30 runs and was generated by random input only.


\subsection{Pearson Correlation}
The Pearson correlation was measured between the two variables \textit{frequency} and \textit{execution duration} based on the evaluation data of the Catch Game. This data only included the runs with an acceleration factor of 1 and an acceleration factor of 10, therefore calculating the correlation based on a tenfold frequency increase. The result of the correlation coefficient was -0.9970277708618712, with a p-value of 2.8192208140850903e-66.

Additionally, the Pearson correlation was also measured between the two variables \textit{parallelization level} and \textit{execution duration} based on the Catch Game's evaluation data. This data only included the runs with no parallelization and a parallelization level of two, therefore calculating the correlation based on parallel executions. The result of the correlation coefficient was -0.9919283060164527, with a p-value of 1.0061646798037734e-53.

All measurements included the overhead of starting and stopping the Headless Chrome instance, as also generating and formatting the summary and coverage output.




\section{Evaluation Conclusion}
All three \textit{*Con} evaluation results, ITCon, GICon, RICon, and the Pearson correlation point to the same conclusion. The usage of parallelization or the acceleration factor directly influences the execution duration of the test-suites. \textbf{RQ1 \flqq How do an increased update frequency and parallelization influence test execution duration?\frqq{}} can, therefore, be answered as follows. Increasing the parallelization level or acceleration factor to modify the update frequency reduces the execution duration roughly by the same factor or level. The implemented acceleration options, therefore, decrease execution duration.

The ITCon results also answer \textbf{RQ2 \flqq How do an increased update frequency and parallelization influence the test-failure rate?\frqq{}}. As expected in the \textit{Analysis} chapter, increasing the acceleration factor above a safe level increases the test-failure rate. Especially when using the acceleration factor in combination with parallelization, increased test-failure rates could also be observed with lower acceleration factors. The \textit{safe level} depends on the hardware the tests are executed on. The author was able to use an acceleration factor of 10, without increasing the test-failure rate on the described platform. Acceleration can, therefore, be used while not increasing the test-failure rate, also with caution.

The RICov results answer \textbf{RQ3 \flqq How does an increased update frequency influence the test coverage?\frqq{}}. For most of the tested applications, coverage was generated faster when using an increased acceleration factor. Only for those applications which already achieved a 100 \% coverage after only one second, the increased acceleration factor did not decrease execution duration, as the granularity of the measurement was once a second. Herefore, increasing coverage by increasing the acceleration factor is possible.
